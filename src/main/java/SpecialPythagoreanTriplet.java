import org.junit.Assert;

public class SpecialPythagoreanTriplet {

    public static void main(String[] args) {
        tests(31875000);
    }

    private static int findPythagoreanProduct() {
        int product = 1;

        for (int a = 3; a < 1000; a++) {
            for (int b = a + 1; b < 1000; b++) {
                double cSquared = (a*a) + (b*b);
                double c = Math.sqrt(cSquared);

                if (a + b + c == 1000) {
                    product = (int) (a * b * c);
                }
            }
        }
        System.out.println("When a+b+c=1000, then the product of a,b,c is: " + product);

        return product;
    }

    private static void tests(int product) {
        Assert.assertEquals(findPythagoreanProduct(), product);
    }
}
